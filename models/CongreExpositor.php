<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_expositor".
 *
 * @property integer $idExpositor
 * @property string $Nombre
 * @property string $dni
 * @property string $Institucion
 * @property string $Telefono
 * @property string $Mail
 *
 * @property CongreExporesumen[] $congreExporesumens
 */
class CongreExpositor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_expositor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'dni', 'Institucion', 'Telefono', 'Mail'], 'required'],
            [['Nombre', 'dni', 'Institucion', 'Telefono', 'Mail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idExpositor' => 'Nombre del Autor',
            'Nombre' => 'Nombre',
            'dni' => 'DNI',
            'Institucion' => 'Institucion',
            'Telefono' => 'Telefono',
            'Mail' => 'eMail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongreExporesumens()
    {
        return $this->hasMany(CongreExporesumen::className(), ['idExpositor' => 'idExpositor']);
    }
}
