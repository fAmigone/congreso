<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_resumen;

/**
 * CongreResumenSearch represents the model behind the search form about `app\models\Congre_resumen`.
 */
class CongreResumenSearch extends Congre_resumen
{
    public function rules()
    {
        return [
            [['idresumen', 'idcategoria'], 'integer'],
            [['Titulo', 'Comentario'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_resumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idresumen' => $this->idresumen,
            'idcategoria' => $this->idcategoria,
        ]);

        $query->andFilterWhere(['like', 'Titulo', $this->Titulo])
            ->andFilterWhere(['like', 'Comentario', $this->Comentario]);

        return $dataProvider;
    }
}
