<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_codigosusados".
 *
 * @property string $codigo
 */
class Congre_codigosusados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_codigosusados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'string', 'max' => 6],
            [['codigo'], 'unique'],            
            [['codigo'], 'exist', 'targetClass'=> 'app\models\Congre_codigos', 'targetAttribute'=> 'idcodigo', 'message'=> 'El Código ingresado no es válido'],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código de Verificación',
        ];
    }
}
