<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_autor".
 *
 * @property integer $idAutor
 * @property integer $idResumen
 * @property string $Nombre
 * @property string $dni
 * @property string $Institucion
 * @property string $Telefono
 * @property string $Mail
 *
 * @property CongreResumen $idResumen0
 */
class Congre_autor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_autor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['idResumen', 'Apellido', 'Nombre', 'Institucion'], 'required'],
            [['idResumen'], 'integer'],
            [['Nombre', 'Apellido','dni', 'Institucion', 'Telefono', 'Mail'], 'string', 'max' => 80]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idAutor' => 'Id Autor',
            'idResumen' => 'Id Resumen',
            'Apellido' => 'Apellido',
            'Nombre' => 'Nombres',
            'dni' => 'Dni',
            'Institucion' => 'Institución',
            'Telefono' => 'Telefono',
            'Mail' => 'Mail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdResumen0()
    {
        return $this->hasOne(CongreResumen::className(), ['idresumen' => 'idResumen']);
    }
}
