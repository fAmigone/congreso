<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CongreExporesumen;

/**
 * CongreExporesumenSearch represents the model behind the search form about `app\models\CongreExporesumen`.
 */
class CongreExporesumenSearch extends CongreExporesumen
{
    public function rules()
    {
        return [
            [['idExporesumen', 'idresumen', 'idExpositor'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CongreExporesumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idExporesumen' => $this->idExporesumen,
            'idresumen' => $this->idresumen,
            'idExpositor' => $this->idExpositor,
        ]);

        return $dataProvider;
    }
}
