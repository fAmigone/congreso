<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_resumen;

/**
 * Congre_resumenSearch represents the model behind the search form about `app\models\Congre_resumen`.
 */
class Congre_resumenSearch extends Congre_resumen
{
    public function rules()
    {
        return [
            [['idresumen', 'idcategoria'], 'integer'],
            //[['Titulo', 'Comentario'], 'safe'],
            [['Titulo', 'Comentario', 'PalabraClave1', 'PalabraClave2', 'PalabraClave3', 'mailautor','PalabraClave4'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_resumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idresumen' => $this->idresumen,
            'idcategoria' => $this->idcategoria,
        ]);

        $query->andFilterWhere(['like', 'Titulo', $this->Titulo])
            ->andFilterWhere(['like', 'Comentario', $this->Comentario])   
            ->andFilterWhere(['like', 'mailautor', $this->mailautor])
            ->andFilterWhere(['like', 'PalabraClave1', $this->PalabraClave1])
            ->andFilterWhere(['like', 'PalabraClave2', $this->PalabraClave2])
            ->andFilterWhere(['like', 'PalabraClave3', $this->PalabraClave3])
            ->andFilterWhere(['like', 'PalabraClave4', $this->PalabraClave4]);


        return $dataProvider;
    }
}
