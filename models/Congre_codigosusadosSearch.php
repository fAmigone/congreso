<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_codigosusados;

/**
 * Congre_codigosusadosSearch represents the model behind the search form about `app\models\Congre_codigosusados`.
 */
class Congre_codigosusadosSearch extends Congre_codigosusados
{
    public function rules()
    {
        return [
            [['codigo'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_codigosusados::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'codigo', $this->codigo]);

        return $dataProvider;
    }
}
