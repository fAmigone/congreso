<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_categoria;

/**
 * CongreCategoriaSearch represents the model behind the search form about `app\models\Congre_categoria`.
 */
class CongreCategoriaSearch extends Congre_categoria
{
    public function rules()
    {
        return [
            [['idcategoria'], 'integer'],
            [['categoria'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_categoria::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idcategoria' => $this->idcategoria,
        ]);

        $query->andFilterWhere(['like', 'categoria', $this->categoria]);

        return $dataProvider;
    }
}
