<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_categoria".
 *
 * @property integer $idcategoria
 * @property string $categoria
 *
 * @property CongreResumen[] $congreResumens
 */
class Congre_categoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_categoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria'], 'required'],
            [['categoria'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcategoria' => 'Idcategoria',
            'categoria' => 'Area Tematica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongreResumens()
    {
        return $this->hasMany(CongreResumen::className(), ['idcategoria' => 'idcategoria']);
    }
}
