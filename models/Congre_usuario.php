<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_usuario".
 *
 * @property integer $idUsuario
 * @property string $Apellido
 * @property string $Nombre
 * @property string $dni
 * @property string $Institucion
 * @property string $Celular
 * @property string $Mail
 * @property string $Pais
 * @property string $Provincia
 * @property string $Ciudad
 */
class Congre_usuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Apellido', 'Nombre', 'Institucion'], 'required'],
            [['Apellido'], 'string', 'max' => 100],
            [['Mail'], 'email'],
            [['Nombre', 'dni', 'Institucion', 'Celular', 'Mail', 'Pais', 'Provincia', 'Ciudad'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'Apellido' => 'Apellido',
            'Nombre' => 'Nombre',
            'dni' => 'DNI',
            'Institucion' => 'Institución o Empresa',
            'Celular' => 'Teléfono',
            'Mail' => 'eMail',
            'Pais' => 'País',
            'Provincia' => 'Provincia',
            'Ciudad' => 'Ciudad',
        ];
    }
}
