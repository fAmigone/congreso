<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_exporesumen".
 *
 * @property integer $idExporesumen
 * @property integer $idresumen
 * @property integer $idExpositor
 *
 * @property CongreResumen $idresumen0
 * @property CongreExpositor $idExpositor0
 */
class CongreExporesumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_exporesumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idresumen', 'idExpositor'], 'required'],
            [['idresumen', 'idExpositor'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idExporesumen' => 'Id Exporesumen',
            'idresumen' => 'Idresumen',
            'idExpositor' => 'Nombre del Expositor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    //public function getIdresumen0()
    public function getidresumen0()
    {
        return $this->hasOne(CongreResumen::className(), ['idresumen' => 'idresumen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getidExpositor0()
    {
        return $this->hasOne(CongreExpositor::className(), ['idExpositor' => 'idExpositor']);
    }
}
