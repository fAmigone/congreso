<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_codigos".
 *
 * @property string $idcodigo
 */
class Congre_codigos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_codigos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcodigo'], 'required'],
            [['idcodigo'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcodigo' => 'Idcodigo',
        ];
    }
}
