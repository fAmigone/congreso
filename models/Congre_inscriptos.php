<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_inscriptos".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $mail
 * @property integer $idcurso
 */
class Congre_inscriptos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_inscriptos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'mail', 'idcurso'], 'required'],
            [['idcurso'], 'integer'],
            [['mail'], 'email'],
            [['nombre', 'apellido', 'mail'], 'string', 'max' => 300],
            [['mail'], 'exist', 'targetClass'=> 'app\models\Congre_usuario', 'targetAttribute'=> 'Mail', 'message'=> 'Usted debe inscribirse primero en el congreso'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'mail' => 'Mail',
            'idcurso' => 'Curso',
        ];
    }
}
