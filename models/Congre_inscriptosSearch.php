<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_inscriptos;

/**
 * Congre_inscriptosSearch represents the model behind the search form about `app\models\Congre_inscriptos`.
 */
class Congre_inscriptosSearch extends Congre_inscriptos
{
    public function rules()
    {
        return [
            [['id', 'idcurso'], 'integer'],
            [['nombre', 'apellido', 'mail'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_inscriptos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idcurso' => $this->idcurso,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'mail', $this->mail]);

        return $dataProvider;
    }
}
