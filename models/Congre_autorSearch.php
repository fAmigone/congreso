<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_autor;

/**
 * Congre_autorSearch represents the model behind the search form about `app\models\Congre_autor`.
 */
class Congre_autorSearch extends Congre_autor
{
    public function rules()
    {
        return [
            [['idAutor', 'idResumen'], 'integer'],
            [['Nombre', 'Apellido', 'dni', 'Institucion', 'Telefono', 'Mail'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_autor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idAutor' => $this->idAutor,
            'idResumen' => $this->idResumen,
        ]);

        $query->andFilterWhere(['like', 'Apellido', $this->Apellido])
            ->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'Institucion', $this->Institucion])
            ->andFilterWhere(['like', 'Telefono', $this->Telefono])
            ->andFilterWhere(['like', 'Mail', $this->Mail]);

        return $dataProvider;
    }
}
