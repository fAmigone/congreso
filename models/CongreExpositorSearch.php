<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CongreExpositor;

/**
 * CongreExpositorSearch represents the model behind the search form about `app\models\CongreExpositor`.
 */
class CongreExpositorSearch extends CongreExpositor
{
    public function rules()
    {
        return [
            [['idExpositor'], 'integer'],
            [['Nombre', 'dni', 'Institucion', 'Telefono', 'Mail'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CongreExpositor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idExpositor' => $this->idExpositor,
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'Institucion', $this->Institucion])
            ->andFilterWhere(['like', 'Telefono', $this->Telefono])
            ->andFilterWhere(['like', 'Mail', $this->Mail]);

        return $dataProvider;
    }
}
