<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_usuario;

/**
 * Congre_usuarioSearch represents the model behind the search form about `app\models\Congre_usuario`.
 */
class Congre_usuarioSearch extends Congre_usuario
{
    public function rules()
    {
        return [
            [['idUsuario'], 'integer'],
            [['Apellido', 'Nombre', 'dni', 'Institucion', 'Celular', 'Mail', 'Pais', 'Provincia', 'Ciudad'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_usuario::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idUsuario' => $this->idUsuario,
        ]);

        $query->andFilterWhere(['like', 'Apellido', $this->Apellido])
            ->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'Institucion', $this->Institucion])
            ->andFilterWhere(['like', 'Celular', $this->Celular])
            ->andFilterWhere(['like', 'Mail', $this->Mail])
            ->andFilterWhere(['like', 'Pais', $this->Pais])
            ->andFilterWhere(['like', 'Provincia', $this->Provincia])
            ->andFilterWhere(['like', 'Ciudad', $this->Ciudad]);

        return $dataProvider;
    }
}
