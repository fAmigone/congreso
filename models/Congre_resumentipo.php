<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_resumentipo".
 *
 * @property integer $idresumentipo
 * @property string $tipo
 *
 * @property CongreResumen[] $congreResumens
 */
class Congre_resumentipo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_resumentipo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idresumentipo' => 'Idresumentipo',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongreResumens()
    {
        return $this->hasMany(CongreResumen::className(), ['idresumentipo' => 'idresumentipo']);
    }
}
