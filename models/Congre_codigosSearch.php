<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_codigos;

/**
 * Congre_codigosSearch represents the model behind the search form about `app\models\Congre_codigos`.
 */
class Congre_codigosSearch extends Congre_codigos
{
    public function rules()
    {
        return [
            [['idcodigo'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_codigos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'idcodigo', $this->idcodigo]);

        return $dataProvider;
    }
}
