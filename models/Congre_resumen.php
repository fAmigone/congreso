<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_resumen".
 *
 * @property integer $idresumen
 * @property string $Titulo
 * @property string $Comentario
 * @property integer $idcategoria
 *
 * @property CongreExporesumen[] $congreExporesumens
 * @property CongreCategoria $idcategoria0
 */
class Congre_resumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congre_resumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Titulo', 'Comentario', 'idcategoria', 'mailautor'], 'required',],
           // [['Titulo', 'Comentario', 'idcategoria',], 'required',],            
            [['Comentario'], 'string', 'max' => 2000],
            [['mailautor'], 'email'],
            [['idcategoria', 'idresumentipo'], 'integer'],
            [['Titulo'], 'string', 'max' => 500],
            [['PalabraClave1', 'PalabraClave2', 'PalabraClave3', 'PalabraClave4'], 'string', 'max' => 20],
//            [['clave'], 'string', 'max' => 6],
  //          [['clave'], 'exist', 'targetClass'=> 'app\models\Congre_codigos', 'targetAttribute'=> 'idcodigo', 'message'=> 'El Código ingresado no es válido'],
    //        [['clave'], 'unique'],
           // [['clave'], 'validaclave']
        ];
    }
  /*  
       public function validaclave()
    {
        
        if ($this->clave == NULL) {
            $this->addError('clave', 'Debe ingresar un codigo');
        }
    } */

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idresumen' => 'Idresumen',
            'Titulo' => 'Título',
            'Comentario' => 'Resumen',
            'idcategoria' => 'Área Temática',
	    'PalabraClave1' => 'Palabra Clave #1',
            'PalabraClave2' => 'Palabra Clave #2',
            'PalabraClave3' => 'Palabra Clave #3',
            'PalabraClave4' => 'Palabra Clave #4', 
            'idresumentipo' => 'Tipo de Resumen',
            'mailautor' => 'E-mail del Autor Responsable',
            'clave' => 'Código de Verificación',
        ];
    }
	   public function getIdresumentipo0() 
   { 
       return $this->hasOne(Congre_resumentipo::className(), ['idresumentipo' => 'idresumentipo']); 
   } 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongreExporesumens()
    {
        return $this->hasMany(CongreExporesumen::className(), ['idresumen' => 'idresumen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcategoria0()
    {
        return $this->hasOne(Congre_categoria::className(), ['idcategoria' => 'idcategoria']);
    }
    
                 
}
