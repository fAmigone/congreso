<?php

namespace app\controllers;
//use yii\bootstrap\Modal;
use Yii;
use app\models\Congre_resumen;
use app\models\CongreResumenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\bootstrap\Modal;
use \app\models\Congre_codigos;
/**
 * CongreResumenController implements the CRUD actions for Congre_resumen model.
 */
class CongreResumenController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Congre_resumen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CongreResumenSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

     public function actionCreate($id)
    {
        $model = new Congre_resumen;
        //$searchExpositorModel = new \app\models\CongreExporesumenSearch;
        //$searchUsuarioModel->idProyecto=$id;
       // $dataExpositorProvider = $searchExpositorModel->search(['CongreExporesumenSearch'=>['idresumen'=>$id]]);//Yii::$app->request->getQueryParams());
       // $modelAutor = $this->crearExpositor();
        try {
            
        if ($model->load(Yii::$app->request->post())) {
            //$model->clave= 'temp';
            //$model->clave=$modelcodigo->codigo;
            //$model->clave=$id;
            $model->save();
            //return $this->redirect(['view', 'id' => $model->idresumen,] );
            return $this->redirect(['view', 'id' => $model->idresumen, 'codigo'=>$id] );
            //return $this->redirect(['view', 'id' => $model->idresumen]);
        } else {            
             //return $this->render('create', [
               return $this->render('create', [
                'model' => $model,
                //'codigoOk' => true,//$model->beforeSave(),   
            ]);
        }
        
        } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    
    /**
     * Displays a single Congre_resumen model.
     * @param integer $id
     * @return mixed
     */
    //public function actionView($id)
    public function actionView($id, $codigo)
    {
        
        
        //$searchExpositorModel = new \app\models\CongreExporesumenSearch;        
        //$dataExpositorProvider = $searchExpositorModel->search(['CongreExporesumenSearch'=>['idresumen'=>$id]]);//Yii::$app->request->getQueryParams());
        $searchExpositorModel = new \app\models\Congre_autorSearch;          
        $dataExpositorProvider = $searchExpositorModel->search(['Congre_autorSearch'=>['idResumen'=>$id]]);//Yii::$app->request->getQueryParams());        
        $modelAutor = $this->crearExpositor($id);
        //$modelResumen = $this->actualizarResumen($id);
        $model = $this->findModel($id);
        //$model->clave= NULL;
        //$modelResumen->clave= NULL;
        return $this->render('view', [
                    'model' => $model,
                    'modelAutor' => $modelAutor,
                    'dataExpositorProvider' => $dataExpositorProvider,
                    'searchExpositorModel' => $searchExpositorModel,
                    'codigo' => $codigo,
                   // 'modelResumen' => $modelResumen,
                    //'Controlador' =>$this,
        ]);
       
        
    }
       protected function crearExpositor($idresumen) {
        $model = new \app\models\Congre_autor;
        $model->idResumen = $idresumen;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    protected function actualizarResumen($idresumen) {
        //aqui instanciamos un registro nuevo y lo cargamos.
       
            //$model = new \app\models\Congre_resumen;
            //$model->idresumen = $idresumen;
            $model = $this->findModel($idresumen);                         
           // $model->isNewRecord=false;
         if ($model->load(Yii::$app->request->post())) { 
                    //instanciamos un codigo
                    
                    $unCodigo = new Congre_codigos();
                    //buscamos        
                    $unCodigo = Congre_codigos::findOne($model->clave);                    
                    if ($unCodigo !== NULL) {
                        $model->save();                                         
                         return $this->redirect(['update', 'id' => $model->idresumen]);                                                                        
                    }
		    else {
			if ($unCodigo == NULL)
				{
				$model->save();	                                                                
				}		
			}	    
            }         
        return $model;
    }
    /**
     * Creates a new Congre_resumen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   

    /**
     * Updates an existing Congre_resumen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
  
  public function actionUpdate($id, $codigo)
    {
        $model = $this->findModel($id);  
        $model->clave = $codigo;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->clave == NULL) {
                return $this->redirect(['errorblanco','id'=>$id]);
               
                 
            }
            else {
            return $this->redirect(['errorcodigo',]);}
        } else {
            return $this->render('update', [
                'model' => $model,
                'codigo'=>$codigo,
            ]);
        }
    }
   
  /*   public function actionUpdate($id)
    {
        $model = $this->findModel($id);        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           
            return $this->redirect(['errorcodigo',]); }
         else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
  */  
    /**
     * Deletes an existing Congre_resumen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

      public function actionErrorcodigo()
    {        
            return $this->render('Errorcodigo');
    }
  public function actionErrorblanco($id)
    {        
            return $this->render('Errorblanco', [
                'id' => $id,
            ]);
    }
    /**
     * Finds the Congre_resumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Congre_resumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Congre_resumen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function beforeSave($insert)
{
    if (parent::beforeSave($insert)) {
 
        return true;
    } else {
        return false;
    }
}
    
}
