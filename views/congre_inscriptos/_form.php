<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_inscriptos $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-inscriptos-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <p class="help-block">Seleccione el curso al que se inscribirá</p>
    <?= $form->field($model, 'idcurso')->dropDownList(ArrayHelper::map(\app\models\Congre_cursos::find()->all(),'id','nombre'))?>           
    
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 300]) ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => 300]) ?>

    <?= $form->field($model, 'mail')->textInput(['maxlength' => 300]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <p class="help-block">Cupo (30 personas) – Lugares Disponibles NN</p>
</div>
