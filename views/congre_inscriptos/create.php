<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_inscriptos $model
 */

$this->title = 'Inscripción Cursos Pre Congreso';
//$this->params['breadcrumbs'][] = ['label' => 'Congre Inscriptos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-inscriptos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
