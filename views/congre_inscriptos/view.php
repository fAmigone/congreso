<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_inscriptos $model
 */

$this->title = "Inscripcion Nro: ".$model->id;
?>
<div class="congre-inscriptos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellido',
            'mail',

        ],
    ]) ?>

</div>
