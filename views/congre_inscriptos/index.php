<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Congre_inscriptosSearch $searchModel
 */

$this->title = 'Congre Inscriptos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-inscriptos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Congre Inscriptos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellido',
            'mail',
            'idcurso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
