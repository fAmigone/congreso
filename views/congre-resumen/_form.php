<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumen $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<fieldset>
        <legend> </legend>
            
    </fieldset>
<div class="congre-resumen-form">

    <?php $form = ActiveForm::begin();?>

    <?= $form->field($model, 'Titulo')->textInput(['maxlength' => 500]) ?>
    <p class="help-block">150 caracteres con espacios incluídos.</p>
    <?= $form->field($model, 'Comentario')->textarea(['rows' => 6, 'maxlength' => 2000]) ?>
    <p class="help-block">2000 caracteres con espacios incluídos.</p>
    <?= $form->field($model, 'idcategoria')->dropDownList(ArrayHelper::map(\app\models\Congre_categoria::find()->all(),'idcategoria','categoria'))?>    
    
    <?= $form->field($model, 'idresumentipo')->dropDownList(ArrayHelper::map(\app\models\Congre_resumentipo::find()->all(),'idresumentipo','tipo'))?>           
    <p class="help-block">
    La presentación en modalidad Oral o Póster estará sujeta a consideración del Comité Evaluador Científico. En el caso de que sea necesario una modificación respecto a dicha elección, el Comité se comunicará debidamente con usted.
    </p>
      <?= $form->field($model, 'mailautor')->textInput(['maxlength' => 100]) ?>
      
    <div class="form-inline">
    </div>
    <div class="input-group">
        <div class="form-inline">
                  
                    <?= $form->field($model, 'PalabraClave1')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave2')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave3')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave4')->textInput(['maxlength' => 20]) ?>
                    <p class="help-block">20 caracteres.</p>                 
                    
           </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
