

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autor $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-autor-form">

    <?php $form = ActiveForm::begin(); ?>    
    <?= $form->field($model, 'Apellido')->textInput(['maxlength' => 80]) ?>
    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 80]) ?>
    <?= $form->field($model, 'Institucion')->textInput(['maxlength' => 80]) ?>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Agregar' : 'Agregar Autor', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


