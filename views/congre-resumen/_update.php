<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumen $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<fieldset>
        <legend> </legend>
            
    </fieldset>
<div class="congre-resumen-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="input-group">
        <div class="form-inline">
                    <?= $form->field($model, 'clave')->textInput(['maxlength' => 6]) ?>
                    <span class="help-block">Ingrese aquí el código de 6 caracteres que le fue proporcionado via correo electrónico.</span>                    
                    
           </div>   
    </div>
    
    <div class="form-group">        
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Cargar Codigo', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
