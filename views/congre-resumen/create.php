<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumen $model
 */

$this->title = 'Nuevo Resumen';
//$this->params['breadcrumbs'][] = ['label' => 'Listado de Resumenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-resumen-create">

    <h1><?= Html::encode($this->title) ?></h1>
     
    <?= $this->render('_form', [
        'model' => $model,
       // 'codigoOk' => $codigoOk,
    ]) ?>

</div>
