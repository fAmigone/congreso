<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumen $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<blockquote>  
<h1>¿Desea hacer alguna modificación en el Resumen Ingresado?</h1>
</blockquote>
<fieldset>
        <legend> </legend>
            
    </fieldset>
<div class="congre-resumen-form">

    <?php $model->clave = $codigo ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Titulo')->textInput(['maxlength' => 500]) ?>
    <p class="help-block">150 caracteres con espacios incluidos.</p>
    <?= $form->field($model, 'Comentario')->textarea(['rows' => 6]) ?>
    <p class="help-block">2000 caracteres con espacios incluidos.</p>
    <?= $form->field($model, 'idcategoria')->dropDownList(ArrayHelper::map(\app\models\Congre_categoria::find()->all(),'idcategoria','categoria'))?>    
    <div class="form-inline">
    <?= $form->field($model, 'idresumentipo')->dropDownList(ArrayHelper::map(\app\models\Congre_resumentipo::find()->all(),'idresumentipo','tipo'))?>
    </div>
    <div class="input-group">
        <div class="form-inline">
                    <?= $form->field($model, 'PalabraClave1')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave2')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave3')->textInput(['maxlength' => 20]) ?>
                     <p class="help-block">20 caracteres.</p>
                    <?= $form->field($model, 'PalabraClave4')->textInput(['maxlength' => 20]) ?>
                    <p class="help-block">20 caracteres.</p>                 
                    <?= $form->field($model, 'clave')->textInput(['maxlength' => 6, 'disabled' => true]) ?>                    
                    <span class="help-block">Ingrese aquí el código de 6 caracteres que le fue proporcionado via correo electrónico.</span>
                    
           </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Guardar Cambios y Finalizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
