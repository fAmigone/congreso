<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CongreExpositor;
use app\models\Funciones;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="usuario-proyecto-funcion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idExpositor')->dropDownList(ArrayHelper::map(CongreExpositor::find()->all(),'idExpositor','Nombre'))?>

    <?= ''//$form->field($model, 'idProyecto')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
