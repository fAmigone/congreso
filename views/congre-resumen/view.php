<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;


/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumen $model
 */

$this->title = $model->Titulo;
//$this->params['breadcrumbs'][] = ['label' => 'Listado de Resumenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-resumen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
  
        
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Titulo',
            'idcategoria0.categoria',
            'idresumentipo0.tipo',
            'Comentario:ntext',
            'mailautor',
            'PalabraClave1',
            'PalabraClave2',
            'PalabraClave3',
            'PalabraClave4',
            
        ],
    ]) ?>
    
    <fieldset>
        <legend> <h2>Por favor, ingrese los Autores del Trabajo</h2> 
        
        </legend>
    </fieldset>
    
    
    
     <?php 
        $modelAutor->Nombre = NULL;
        $modelAutor->Apellido = NULL;
        $modelAutor->Institucion = NULL;
        echo $this->render('_formusuario', [
        'model' => $modelAutor,
    ]);
        ?>
    <p class="help-block">Ingresar Autores por Orden de Trabajo.</p>
     
     <h2>Autores vinculados al Trabajo</h2>
        
     <?= GridView::widget([
        'dataProvider' => $dataExpositorProvider,
        'filterModel' => $searchExpositorModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'idExpositor0.Nombre',
            'Apellido', 
            'Nombre',
            'Institucion',            
                
        ],
         
    ]); ?>

     <?= Html::a('Previsualizar Resumen', ['update', 'id' => $model->idresumen, 'codigo'=>$codigo], ['class' => 'btn btn-primary']) ?>
</div>
