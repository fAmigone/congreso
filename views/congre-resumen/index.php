<?php

use yii\helpers\Html;
use yii\grid\GridView;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Congre_resumenSearch $searchModel
 */

$this->title = 'Listado de Resumenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-resumen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Ingresar Resumen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Titulo',
            'Comentario:ntext',
            'idcategoria0.categoria',
            'idresumentipo0.tipo',
            'mailautor',
           'PalabraClave1',
           'PalabraClave2',
           'PalabraClave3',
           'PalabraClave4',


            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}',
                ]
        ],
    ]); ?>

</div>
