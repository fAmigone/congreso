<?php
use yii\helpers\Html;
?>

<blockquote>

<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="error" aria-hidden="true">&times;</button>
  <strong>Atencion!</strong> Su resumen aun no a sido cargado.
</div>
    

<h2>Debe ingresar el codigo.</h2>
<h4>El codigo es una clave de habilitacion que le fue enviada por mail al haberse registrado su pago. Por favor, verifique su casilla de correo si aun no lo ha hecho.</h4>

<?= Html::a('Volver', ['update', 'id' => $id], ['class' => 'btn btn-primary']) ?>
</blockquote>