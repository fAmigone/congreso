<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_resumenSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-resumen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idresumen') ?>

    <?= $form->field($model, 'Titulo') ?>

    <?= $form->field($model, 'Comentario') ?>

    <?= $form->field($model, 'idcategoria') ?>

    <?= $form->field($model, 'mailautor') ?>
    
    <?= $form->field($model, 'PalabraClave1') ?> 
 
   <?php $form->field($model, 'PalabraClave2') ?> 
 
   <?php $form->field($model, 'PalabraClave3') ?> 
 
   <?php $form->field($model, 'PalabraClave4') ?> 
 

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
