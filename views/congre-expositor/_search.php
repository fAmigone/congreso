<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExpositorSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-expositor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idExpositor') ?>

    <?= $form->field($model, 'Nombre') ?>

    <?= $form->field($model, 'dni') ?>

    <?= $form->field($model, 'Institucion') ?>

    <?= $form->field($model, 'Telefono') ?>

    <?php // echo $form->field($model, 'Mail') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
