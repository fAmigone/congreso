<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExpositor $model
 */

$this->title = 'Update Congre Expositor: ' . $model->idExpositor;
$this->params['breadcrumbs'][] = ['label' => 'Congre Expositors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idExpositor, 'url' => ['view', 'id' => $model->idExpositor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-expositor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
