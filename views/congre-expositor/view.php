<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExpositor $model
 */

$this->title = $model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Listado de Autores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-expositor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'idExpositor',
            'Nombre',
            'dni',
            'Institucion',
            'Telefono',
            'Mail',
        ],
    ]) ?>

</div>
