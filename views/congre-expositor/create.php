<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExpositor $model
 */

$this->title = 'Ingresar Nuevo Expositor';
$this->params['breadcrumbs'][] = ['label' => 'Listado de Expositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-expositor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
