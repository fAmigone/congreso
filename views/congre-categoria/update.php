<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_categoria $model
 */

$this->title = 'Update Congre Categoria: ' . $model->idcategoria;
$this->params['breadcrumbs'][] = ['label' => 'Congre Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcategoria, 'url' => ['view', 'id' => $model->idcategoria]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-categoria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
