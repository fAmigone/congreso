<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_categoria $model
 */

$this->title = 'Create Congre Categoria';
$this->params['breadcrumbs'][] = ['label' => 'Congre Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-categoria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
