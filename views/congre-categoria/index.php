<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Congre_categoriaSearch $searchModel
 */

$this->title = 'Congre Categorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-categoria-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Congre Categoria', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcategoria',
            'categoria',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
