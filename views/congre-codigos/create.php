<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigos $model
 */

$this->title = 'Create Congre Codigos';
$this->params['breadcrumbs'][] = ['label' => 'Congre Codigos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-codigos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
