<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigos $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-codigos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idcodigo')->textInput(['maxlength' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
