<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigos $model
 */

$this->title = $model->idcodigo;
$this->params['breadcrumbs'][] = ['label' => 'Congre Codigos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-codigos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idcodigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idcodigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcodigo',
        ],
    ]) ?>

</div>
