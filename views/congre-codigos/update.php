<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigos $model
 */

$this->title = 'Update Congre Codigos: ' . $model->idcodigo;
$this->params['breadcrumbs'][] = ['label' => 'Congre Codigos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcodigo, 'url' => ['view', 'id' => $model->idcodigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-codigos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
