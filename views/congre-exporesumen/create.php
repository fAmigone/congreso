<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExporesumen $model
 */

$this->title = 'Create Congre Exporesumen';
$this->params['breadcrumbs'][] = ['label' => 'Congre Exporesumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-exporesumen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
