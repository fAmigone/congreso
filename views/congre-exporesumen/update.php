<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExporesumen $model
 */

$this->title = 'Update Congre Exporesumen: ' . $model->idExporesumen;
$this->params['breadcrumbs'][] = ['label' => 'Congre Exporesumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idExporesumen, 'url' => ['view', 'id' => $model->idExporesumen]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-exporesumen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
