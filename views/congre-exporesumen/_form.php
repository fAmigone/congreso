<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\CongreExporesumen $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-exporesumen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idresumen')->textInput() ?>

    <?= $form->field($model, 'idExpositor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
