<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_usuario $model
 */

$this->title = $model->idUsuario;
//$this->params['breadcrumbs'][] = ['label' => 'Congre Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->Apellido;
?>
<div class="congre-usuario-view">
<blockquote>  

<div class="alert alert-success alert-dismissable">
 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Su Inscripción al V Congreso de SETAC ha sido registrada.</strong> Recuerde que su inscripción definitiva estará sujeta a la confirmación del pago de inscripción, que se concretará enviando el comprobante de pago al email pagos@congresosetacnqn.com.ar.
</div>    
    
    <h1><?= Html::encode($model->Apellido) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [         
            'Apellido',
            'Nombre',
            'dni',
            'Institucion',
            'Celular',
            'Mail',
            'Pais',
            'Provincia',
            'Ciudad',
        ],
    ]) ?>
 <?= Html::a('Modificar Registro', ['update', 'id' => $model->idUsuario], ['class' => 'btn btn-primary']) ?>
</div>
