<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_usuario $model
 */

$this->title = 'Formulario de Registro: ' . $model->idUsuario;
//$this->params['breadcrumbs'][] = ['label' => 'Congre Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idUsuario, 'url' => ['view', 'id' => $model->idUsuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
