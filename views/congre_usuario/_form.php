<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_usuario $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<blockquote>  

<div class="alert alert-warning">
Para realizar su inscripción en el V Congreso de SETAC, por favor, complete el formulario a continuación.
Recuerde que su inscripción definitiva estará sujeta a la confirmación del pago de inscripción, que se concretará enviando el comprobante de pago al email pagos@congresosetacnqn.com.ar. 

    </div>
    
    <div class="alert alert-info">
    Importante: si realiza el pago de una inscripción grupal, indique el Nombre y Apellido completo de cada integrante del grupo. Una vez confirmada su inscripción, recibirá un código de verificación que le será solicitado en el caso de enviar algún resumen al congreso.
    </div>    
</blockquote>
<div class="congre-usuario-form"  style="float:left;">
    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Apellido')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Institucion')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Celular')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Mail')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Pais')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Provincia')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Ciudad')->textInput(['maxlength' => 30]) ?>

        <div class="panel-body">
        <blockquote>
        <div class="alert alert-danger">
Para realizar el pago de inscripción al V Congreso de SETAC Argentina, por favor, realice un depósito o transferencia bancaria a la siguiente cuenta:


<ul class="list-group">
  <li class="list-group-item">Razon Social Completa: Fundacion de la Universidad Nacional del Comahue para el Desarrollo Regional, FUNYDER</li>
  <li class="list-group-item">Domicilio: Buenos Aires 1400, Neuquen Capital, Provincia de Neuquen, Argentina. (8300)</li>
  <li class="list-group-item">Institucion Financiera: Banco Credicoop Cooperativo Limitado, Sucursal 093 Neuquen Capital, </li>
  <li class="list-group-item">Direccion: Juan B Justo 160 Neuquen Capital, Provincia de Neuquen, Argentina. (8300)</li>
  <li class="list-group-item">Tipo de Cuenta: Cta Cte en pesos argentinos: 093/10875-7</li>
  <li class="list-group-item">CBU: 1910093355009301087578</li>
  <li class="list-group-item">CUIT: 33-67268463-9</li>
  <li class="list-group-item">Transferencias Internacionales: BCOOARBA</li>
      
</ul>
Una vez realizado el depósito o transferencia, por favor, envíe algún comprobante al siguiente correo electrónico
pagos@congresosetacnqn.com.ar 

    </div>
            </blockquote>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
