<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigosusados $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<h2> Bienvenido a la página de envío de resúmenes del V Congreso SETAC Argentina. </h2>



<div class="alert alert-success alert-dismissable">
 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
   <strong>Le recordamos</strong> que para poder enviar su resumen, deberá realizar primero su inscripción completando el   
  <a href="http://congresosetacnqn.com.ar/stc/congreso/web/index.php?r=congre_usuario/create" class="alert-link">Formulario de Registro.</a>
   Una vez efectuada la misma, deberá realizar el pago correspondiente según 
   <a href="http://www.congresosetacnqn.com.ar/stc/index.php/inscripciones/inscripcion/aranceles-de-inscripcion" class="alert-link">los Aranceles del Congreso.</a> 
   Finalmente, para que su inscripción sea confirmada, envíe el comprobante de pago al email 
   
<a href="mailto:youremailaddress">pagos@congresosetacnqn.com.ar</a> 
Una vez recepcionado el pago, recibirá un Código de Verificación que le permitirá acceder al Formulario de Envío de Resúmenes. Cada inscripto tendrá derecho al envío de 2 (dos) resúmenes. 
<br/>
<strong>Fecha límite de envío de Resumenes: 15 de Agosto</strong>
</div>  
<blockquote>  
  <div class="bs-callout bs-callout-warning" id="callout-navbar-overflow">      
    <h4>Información general sobre los resúmenes</h4>
    
    <ul type="a">
       <li><strong>Título:</strong> 150 caracteres incluidos espacios.</li>
       <li><strong>Resumen científico:</strong> 2000 caracteres incluidos espacios.</li>
       <li><strong>Palabras claves:</strong> hasta 4 de 20 caracteres incluidos espacios</li>
       <li><strong>E-mail del autor responsable.</strong></li>    
       <li><strong>Área Temática: </strong>
           <ol type="a">
               <li>  Ecotoxicología (agua, suelo y aire). </li>
               <li>  Química Analítica y Ambiental. </li>
               <li>  Regulación, Gestión y Evaluación de Riesgos. </li>
               <li>  Mecanismos de Toxicidad y Factores de estrés y Biomarcadores. </li>
               <li>  Modelados de exposición y efectos. </li>
               <li>  Educación para la gestión ambiental participativa. </li>
               <li>  Ciclos de Vida. </li>
               <li>  Remediación. </li>
               <li>  Otras áreas. </li>
            </ol>
           Autores e Instituciones a las que pertenecen
       <li><strong>Modalidad de la presentación:</strong> Oral o Póster</li>
La presentación en modalidad Oral o Póster estará sujeta a consideración del Comité Evaluador Científico. En el caso de que sea necesario una modificación respecto a dicha elección, el Comité se comunicará debidamente con usted.</li>
</ul>
</blockquote>
    <div class="alert alert-warning">


       <h4><strong>Formatos</strong></h4>
       <ul type="a">
           <li><strong>Presentación Oral:</strong> 10 minutos de presentación + 5 minutos de preguntas</li>

           <li><strong>Presentación de Póster</strong>
        <ul type="a">
            <li> <strong>Formato:</strong> 90 cm de ancho x 120 cm de alto </li>
            <li> <strong>Título:</strong> Debe ser el mismo que el del Resumen que haya sido presentado y aceptado.</li>
            <li> <strong>Autor o Autores:</strong> Los autores que figuren en el póster deben ser los mismos que firmaban el Resumen.</li>
            <li> <strong>Filiación:</strong> Debe incluirse el nombre del Departamento y del Centro, Organismo o Empresa del Autor o los Autores, dirección postal, Ciudad y País donde se realizó el trabajo, así como correo electrónico del autor responsable.</li>
                </li>
        </ul>
        </li>
        </ul>
        </div>
  </div>

    
 <blockquote> 
<h4>Para proceder al envío de su resumen, por favor, ingrese el Código de Verificación que le fue proporcionado con la confirmación de su inscripción.</h4>


<div class="congre-codigosusados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Validar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</blockquote> 