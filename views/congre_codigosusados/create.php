<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_codigosusados $model
 */

//$this->title = 'Bienvenido a la página de envío de resúmenes del V Congreso SETAC Argentina.';
//$this->params['breadcrumbs'][] = ['label' => 'Congre Codigosusados', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-codigosusados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
