<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Congre_codigosusadosSearch $searchModel
 */

$this->title = 'Congre Codigosusados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-codigosusados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Congre Codigosusados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
