<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autor $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-autor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idResumen')->textInput() ?>
    
    <?= $form->field($model, 'Apellido')->textInput(['maxlength' => 100]) ?> 
    
    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Institucion')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Telefono')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Mail')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
