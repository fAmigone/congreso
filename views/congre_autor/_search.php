<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autorSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congre-autor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idAutor') ?>

    <?= $form->field($model, 'idResumen') ?>

    <?= $form->field($model, 'Apellido') ?> 
    
    <?= $form->field($model, 'Nombre') ?>

    <?= $form->field($model, 'dni') ?>

    <?= $form->field($model, 'Institucion') ?>

    <?php // echo $form->field($model, 'Telefono') ?>

    <?php // echo $form->field($model, 'Mail') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
