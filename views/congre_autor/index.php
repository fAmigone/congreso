<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Congre_autorSearch $searchModel
 */

$this->title = 'Congre Autors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-autor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Congre Autor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAutor',
            'idResumen',
            'Apellido', 
            'Nombre',
            'dni',
            'Institucion',
            // 'Telefono',
            // 'Mail',

                 ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',                
                ],
        ],
    ]); ?>

</div>
