<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autor $model
 */

$this->title = $model->idAutor;
$this->params['breadcrumbs'][] = ['label' => 'Congre Autors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-autor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idAutor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idAutor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idAutor',
            'idResumen',
            'Apellido', 
            'Nombre',
            'dni',
            'Institucion',
            'Telefono',
            'Mail',
        ],
    ]) ?>

</div>
