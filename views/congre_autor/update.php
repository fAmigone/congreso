<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autor $model
 */

$this->title = 'Update Congre Autor: ' . $model->idAutor;
$this->params['breadcrumbs'][] = ['label' => 'Congre Autors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAutor, 'url' => ['view', 'id' => $model->idAutor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congre-autor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
