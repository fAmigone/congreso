<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congre_autor $model
 */

$this->title = 'Create Congre Autor';
$this->params['breadcrumbs'][] = ['label' => 'Congre Autors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congre-autor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
